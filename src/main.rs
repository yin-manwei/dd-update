use std::io::*;
use std::fs::OpenOptions;
use std::env::args;

mod rafile;

use rafile::*;

fn drop<T>(_:T){}

fn print_help(a0:&str){
    eprintln!("Usage: {} <source> <destination> <block_size> [skip_blocks [number_of_blocks]]",a0);
}

fn main() {
    // parse argv
    let mut a = args();
    let a0 = a.next().unwrap_or(String::from("$0"));
    let sn = {
        a.next()
        .unwrap_or(String::new())
    };
    let dn = {
        a.next()
        .unwrap_or(String::new())
    };
    let b = {
        a.next()
        .unwrap_or(String::new())
        .parse()
        .unwrap_or(1)
    };
    let mut c = (b as u64) * {
        a.next()
        .unwrap_or(String::new())
        .parse()
        .unwrap_or(0)
    };
    let mut s = vec![0u8;b];
    let mut d = vec![0u8;b];
    let z = match a.next().unwrap_or(String::new()).parse::<u64>() {
        Ok(y) => c + y * (b as u64),
        Err(_) => u64::max_value(),
    };
    if (b < 1) | (&dn == "") {
        return print_help(&a0);
    }
    drop(a);drop(a0);

    // open the files
    let mut sf = BufReader::new({
            let mut f = OpenOptions::new()
            .read(true)
            .open(
                match &sn[..] {
                    // TODO: handle non-POSIX systems
                    "" => "/dev/stdin",
                    "-" => "/dev/stdin",
                    n => n
                }
            ).expect("Could not open source file");
            if c > 0 {
                println!("Skipping/Seeking {} bytes",c);
                f.seek(SeekFrom::Start(c)).expect("cannot seek");
            }
            f
        }
    );
    drop(sn);
    let mut df = RandomAccessFile::from((
        match OpenOptions::new()
            .write(true)
            .read(true)
            .open(
                &dn
            ) {
                Ok(f) => f,
                _ => OpenOptions::new() // if it doesn't exist, create it and try again
                    .write(true)
                    .read(true)
                    .create_new(true)
                    .open(
                        &dn
                    )
                    .expect("Could not open destination file"),
            },
            c // offset in bytes
    ));
    drop(dn);

    // copy the data
    // TODO: skip over read errors
    while let Ok(l) = sf.read(&mut s) {
        // check if destination block really needs to be overwritten
        let w = match df.read_at(c,&mut d) {
            Ok(l) => s[0..l] != d[0..l],
            _ => true,
        };

        if w == true {
            match df.write_at(c,&s[..l]) {
                Ok(l) => println!("Wrote 0x{:016x} bytes at 0x{:016x}",l,c),
                _ => println!("Failed 0x{:016x} bytes at 0x{:016x}",l,c),
            }
        } else {
            println!("Skipped 0x{:016x} bytes at 0x{:016x}",l,c);
        }
        c += l as u64;
        if (l == 0) | (c >= z) { // either EOF or $max_blocks copied
            break;
        }
    }
    println!("Finished after {} bytes",c)
}
