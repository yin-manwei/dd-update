use std::io::*;
use std::fs::File;

pub trait RandomAccessMemory {
    fn read_at(&mut self,addr:u64,buf:&mut[u8]) -> Result<usize>;
    fn write_at(&mut self,addr:u64,buf:&[u8]) -> Result<usize>;
}

pub struct RandomAccessFile {
    pub f:File,
    pub o:u64,
}

impl From<(File,u64)> for RandomAccessFile {
    fn from((file,offset):(File,u64)) -> Self {
        Self {
            f:file,
            o:offset
        }
    }
}

impl From<File> for RandomAccessFile {
    fn from(file:File) -> Self {
        Self::from((file,0))
    }
}


impl RandomAccessMemory for RandomAccessFile {
    fn read_at(&mut self,addr:u64,buf:&mut[u8]) -> Result<usize> {
        self.f.seek(SeekFrom::Start(addr+self.o))?;
        self.f.read(buf)
    }
    fn write_at(&mut self,addr:u64,buf:&[u8]) -> Result<usize> {
        self.f.seek(SeekFrom::Start(addr+self.o))?;
        self.f.write(buf)
    }
}
